<?php get_header()?>

<?php if ( have_posts() ) : ?>
    <div class="container page-container">

        <?php while ( have_posts() ) : the_post(); ?>
        	<div class="row">
        		<div class="col-xs-12 col-md-8 col-md-offset-2">
	        		<h1><?php the_title(); ?></h1>
	                <?php the_content();?>


	            </div>
	        </div>
        <?php endwhile;

        ?>

    </div>
<?php endif; ?>

<?php get_footer() ?>
