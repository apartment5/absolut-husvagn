<!DOCTYPE html>
<!--[if lt IE 7]><html class="no-js lt-ie9 lt-ie8 lt-ie7" <?php language_attributes() ?>><![endif]-->
<!--[if IE 7]><html class="no-js lt-ie9 lt-ie8" <?php language_attributes() ?>><![endif]-->
<!--[if IE 8]><html class="no-js lt-ie9" <?php language_attributes() ?>><![endif]-->
<!--[if gt IE 8]><!--><html class="no-js" <?php language_attributes() ?>><!--<![endif]-->
    <head>
        <meta charset="<?php bloginfo( 'charset' ) ?>">
        <meta name="viewport" content="width=device-width">
        <title><?php echo get_bloginfo( 'name' ); ?><?php wp_title('-'); ?></title>

        <?php wp_head() ?>
    </head>

    <body <?php body_class() ?>>
        <script>
          (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
          (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
          m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
          })(window,document,'script','https://www.google-analytics.com/analytics.js','ga');

          ga('create', 'UA-56406703-18', 'auto');
          ga('send', 'pageview');

        </script>

        <noscript><iframe src="//www.googletagmanager.com/ns.html?id=GTM­NXKCK7"
        height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
        <script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
        new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
        j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
        '//www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
        })(window,document,'script','dataLayer','GTM­NXKCK7');</script>


        <div id="fb-root"></div>
        <script>(function(d, s, id) {
          var js, fjs = d.getElementsByTagName(s)[0];
          if (d.getElementById(id)) return;
          js = d.createElement(s); js.id = id;
          js.src = "//connect.facebook.net/en_US/sdk.js#xfbml=1";
          fjs.parentNode.insertBefore(js, fjs);
        }(document, 'script', 'facebook-jssdk'));</script>

        <div id="avp-overlay"></div>


        <div class="flower-wrapper">
            <img class="bg-flower" id="bg-flower-1" src="<?php echo site_url(); ?>/wp-content/uploads/flower_bottom_small.png"/>
        </div>

        <img class="bg-flower" id="bg-flower-3" src="<?php echo site_url(); ?>/wp-content/uploads/flower_top_big.png"/>

        <img class="bg-flower" id="bg-flower-4" src="<?php echo site_url(); ?>/wp-content/uploads/flower_bottom_small.png"/>

        <img class="bg-flower" id="bg-flower-2" src="<?php echo site_url(); ?>/wp-content/uploads/flower_top_big.png"/>



        <header>
            <div class="header-logo text-center">
                <a href="<?php echo site_url(); ?>">
                <img src="<?php echo site_url(); ?>/wp-content/uploads/2016/05/absolut-logo-white.png" alt="Absolut Logo" height="29"/>
                    </a>
            </div>

            <div id="header-nav">
        		<?php wp_nav_menu(array(
        			'theme_location' => 'header-menu',
        		)) ?>
        	</div>

            <div class="menu-btn">
                <span class="top-line"></span>
                <span class="middle-line"></span>
                <span class="bottom-line"></span>
            </div>

        </header>
