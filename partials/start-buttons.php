<?php if ( have_rows('buttons', 'option') ):?>
	<div class="start-page-buttons">
		<?php while ( have_rows('buttons', 'option') ) : the_row();
			$btntext = get_sub_field('button_text');
			$url = get_sub_field('link');
			$disabled = get_sub_field('disabled');

			if ($url == ("url")) {

                $link = get_sub_field('button_link');

            }elseif ($url == ("page"))  {

                $link = get_sub_field('page_link');
            }
		?>

		<div class="col-xs-12 col-sm-8 col-sm-offset-2">
			<a class="btn btn-primary btn-lg <?php if($disabled == true):?>disabled<?php endif;?>" href="<?php echo $link;?>" <?php echo ($url == 'url' ? ' target="_blank"' : ''); ?>><?php echo $btntext;?> <i class="fa fa-angle-right fa-lg" aria-hidden="true"></i></a>
		</div>

	<?php endwhile;?>
	</div>
<?php endif;?>
