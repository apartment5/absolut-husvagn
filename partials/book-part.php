<?php
$headline = get_field('headline', 'option');
$text = get_field('text_field', 'option');
$btntext = get_field('button_text', 'option');
$btnlink = get_field('button_link', 'option');
?>

<?php if ($btnlink): ?>

<div class="row">
	<div class="booking-wrapper text-center col-xs-12 col-md-8 col-md-offset-2">
		<hr/>
		<h3><?php echo $headline;?></h3>
		<?php echo $text;?>
		<a class="btn btn-primary btn-lg disabled" href="<?php echo $btnlink;?>" target="_blank">
			<?php echo $btntext;?> <i class="fa fa-angle-right fa-lg" aria-hidden="true"></i></i>
		</a>
	</div>
</div>

<?php endif;?>
