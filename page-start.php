<?php

/*
Template Name: Start
 */

get_header() ?>

<div class="page-content">
	<div class="container">
		<div class="row">
			<div class="col-xs-12 col-md-8 col-md-offset-2 text-center">
				<?php if ( have_posts() ):
			            while ( have_posts() ) : the_post();
			                the_content();
			            endwhile;

			        else :
			            echo 'Nothing to show';
			    endif;?>

			    <?php get_template_part( 'partials/start-buttons', 'page' ); ?>
			</div>
		</div>
		<div class="row">
			<div class="col-xs-12 col-md-8 col-md-offset-2 text-center" style="margin-top: 30px">
				<h4>Vill du ha uppdateringar från Absolut?</h4>
				<p>Vi tipsar dig och ger dig förtur till events, konstsamarbeten, drinkar och mycket annat. Prenumerera på vårt nyhetsbrev och kolla in vår site <a href="http://psstevents.com">psstevents.com</a>!</p>

				<div class="row">
					<div class="col-xs-12 col-md-8 col-md-offset-2">
						<?php echo do_shortcode('[contact-form-7 id="186"]'); ?>

						<p class="small">Nyhetsbrevet innehåller information om alkoholhaltiga drycker. Vänligen sprid inte materialet till någon under 25 år.</p>
					</div>
				</div>


			</div>
		</div>
	</div>

</div>


<?php get_footer() ?>
