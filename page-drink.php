<?php

/*
Template Name: Drink
 */

get_header();

if ($_REQUEST['action'] == 'addVote') {
	$votes = (int) get_field('votes');

	if (!isset($_COOKIE['absolut-husvagn-voted-' . get_the_ID()])) {
		$votes++;
		update_field('votes', $votes);
	}

	echo '<div id=new-votes">' . $votes . '</div>';
}

$image = get_field('image');
$option = get_field('option');
$creator = get_field('creator');
$instructions = get_field('instructions');
$liquor = get_field('liquor_info');
$liquor_image = get_field('liquor_image');
$link = get_field('link');

?>

<div class="page-content">
	<div class="container">
		<div class="row">
			<div class="col-xs-12 col-md-8 col-md-offset-2 text-center">
				<h1><?php the_title(); ?></h1>
			</div>

			<div class="col-xs-6 col-xs-offset-3 col-md-4 col-md-offset-4 text-center">
				<img class="drink-image" src="<?php echo $image[url]; ?>"/>
			</div>
		</div>

		<div class="row">
			<div class="col-xs-12 col-md-8 col-md-offset-2 text-center">

				<h4>Ingredienser</h4>

				<?php if ( have_rows('ingredients') ):?>
					<ul class="drink-ingredients">
				        <?php while ( have_rows('ingredients') ) : the_row();?>

				           	<li><?php the_sub_field('ingredient');?></li>

				        <?php endwhile;?>
				    </ul>

				<?php endif;?>

				<?php if($instructions):?>
					<?php echo $instructions;?>
				<?php endif;?>

			</div>
		</div>

		<div class="row">
			<div class="drink-recipe col-xs-12 col-md-8 col-md-offset-2 text-center">
				<h4>Alkoholfritt alternativ</h4>
				<?php echo $option; ?>
			</div>
		</div>

		<div class="row">
			<div class="drink-creator col-xs-12 col-md-8 col-md-offset-2 text-center">
				<em><small>Skapad av: <?php echo $creator; ?></small></em>
			</div>
		</div>

		<div class="row">
			<div class="drink-like col-xs-12 col-md-8 col-md-offset-2 text-center">
				<h4>Gilla och dela denna drink</h4>
					<div class="like">
						<button class="btn btn-vote" id="add-vote" data-id="<?php the_ID(); ?>">
							<?php if (!isset($_COOKIE['absolut-husvagn-voted-' . get_the_ID()])) { ?>
								<i class="fa fa-heart-o" aria-hidden="true"></i>
								<i class="fa fa-heart hide" aria-hidden="true"></i>
							<?php } else { ?>
								<i class="fa fa-heart" aria-hidden="true"></i>
							<?php } ?>
		 				</button>
		 				<p class="votes" id="votes"><?php the_field('votes'); ?></p>
	 				</div>

	 				<div class="social">
						<a href="https://www.facebook.com/sharer/sharer.php?u=<?php the_permalink(); ?>">
							<i class="fa fa-facebook" aria-hidden="true"></i>
						</a>
					</div>
					<div class="social">
						<a href="https://twitter.com/home?status=<?php the_permalink(); ?>"><i class="fa fa-twitter" aria-hidden="true"></i></a>
					</div>
			</div>
		</div>

		<div class="row">
			<div class="col-xs-12 col-md-8 col-md-offset-2 text-center">
				<div class="drink-about">
					<?php echo $liquor;?>
				</div>
			</div>
			<div class="col-xs-12 col-md-4 col-md-offset-4 text-center">
				<img class="liquor-image" src="<?php echo $liquor_image[url]; ?>"/>
			</div>
			<div class="col-xs-12 col-md-8 col-md-offset-2 text-center">
				<a class="btn btn-primary btn-lg" href="<?php echo $link;?>" target="_blank">Köp på systembolaget.se <i class="fa fa-angle-right fa-lg" aria-hidden="true"></i></a>
			</div>
		</div>


		<?php get_template_part( 'partials/book-part', 'page' ); ?>

	</div>

</div>


<?php get_footer() ?>
