		<footer>
			<div class="row">
				<strong>Dela:</strong>
				<div class="col-xs-12 social">
					<a href="https://www.facebook.com/sharer/sharer.php?u=http%3A//absoluthusvagn.se/">
						<i class="fa fa-facebook" aria-hidden="true"></i>
					</a>
					<a href="https://twitter.com/home?status=http%3A//absoluthusvagn.se/"><i class="fa fa-twitter" aria-hidden="true"></i></a>
				</div>
				<div class="col-xs-12">
					<a href="http://www.absolut.com/se/Enjoy-Responsibly">Enjoy Responsibly</a>
				</div>
			</div>
		</footer>
		<?php wp_footer() ?>
    </body>
</html>
