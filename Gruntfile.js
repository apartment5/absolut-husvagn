'use strict';

module.exports = function (grunt) {

    // Load grunt tasks automatically
    require('load-grunt-tasks')(grunt);

    // Time how long tasks take. Can help when optimizing build times
    require('time-grunt')(grunt);

    // Configurable paths
    var config = {
        dist: 'dist'
    };

    // Define the configuration for all the tasks
    grunt.initConfig({

        // Project settings
        config: config,

        // Watches files for changes and runs tasks based on the changed files
        watch: {
            sass: {
                files: ['styles/{,*/}*.{scss,sass}'],
                tasks: ['sass', 'autoprefixer']
            },

            php: {
                files: ['{,*/}*.php']
            },

            livereload: {
                options: {
                    livereload: true
                },
                files: [
                    '*.php',
                    'partials/*.php',
                    '*.css',
                    'images/{,*/}*.{png,gif,jpg}',
                    'scripts/*.js'
                ]
            }
        },

        // Compiles Sass to CSS and generates necessary files if requested
        sass: {
            options: {
                loadPath: [
                    'bower_components'
                ]
            },
            default: {
                files: [{
                    expand: true,
                    sourcemap: 'none',
                    cwd: 'styles',
                    src: ['*.scss'],
                    ext: '.css'
                }]
            }
        },

        // Minify CSS
        // NOT DONE
        cssmin: {
            dist: {
                files: {
                    'style.css': [
                        'style.css'
                    ]
                }
            }
        },

        // Add vendor prefixed styles
        autoprefixer: {
            options: {
                browsers: ['last 1 version']
            },
            dist: {
                files: [{
                    expand: true,
                    // cwd: '.tmp/styles/',
                    src: '{,*/}*.css',
                    // dest: '.tmp/styles/'
                }]
            }
        },

        // The following *-min tasks produce minified files in the dist folder
        // NOT DONE
        imagemin: {
            options: { cache: false },
            dist: {
                files: [{
                    expand: true,
                    cwd: '<%= config.app %>/images',
                    src: '{,*/}*.{gif,jpeg,jpg,png}',
                    dest: '<%= config.dist %>/images'
                }]
            }
        },

        // Clean out dist folder
        clean: ["dist/"],

        // Copy files for build out
        copy: {
            dist: {
                files: [{
                    expand: true,
                    dot: true,
                    // cwd: '/',
                    dest: '<%= config.dist %>',
                    src: [
                        '*.{ico,png,txt,css}',
                        '.htaccess',
                        'images/{,*/}*.*',
                        'scripts/{,*/}*.*',
                        '{,*/}*.php',
                        'fonts/{,*/}*.*',
                        'static/{,*/}*.*',
                    ]
                }]
            }
        }
    });

    grunt.registerTask('build', [
        'clean',
        'sass',
        'autoprefixer:dist',
        'copy:dist'
    ]);
};
