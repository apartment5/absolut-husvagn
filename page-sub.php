<?php

/*
Template Name: Sub Page
 */

get_header() ?>

<div class="page-content">
	<div class="container">
		<div class="row">
			<div class="col-xs-12 col-md-8 col-md-offset-2 text-center">
				<?php if ( have_posts() ):
			            while ( have_posts() ) : the_post();
			                the_content();
			            endwhile;

			        else :
			            echo 'Nothing to show';
			    endif;?>
			</div>
		</div>

		<?php get_template_part( 'partials/book-part', 'page' ); ?>

	</div>

</div>


<?php get_footer() ?>
