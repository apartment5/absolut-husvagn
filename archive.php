<?php

get_header() ?>

<div class="page-content">
    <div class="container">

    <h1>Test</h1>

        <div class="row">
            <?php if ( have_posts() ) : ?>


                <?php query_posts($query_string . '&orderby=title&order=ASC');

                    while ( have_posts() ) : the_post(); ?>
                        <div class="row custom-post-wrapper">

                            <div class="row">
                                <div class="col-xs-12 col-md-8 col-md-offset-2">
                                    <h3>
                                        <a class="" href="<?php the_permalink(); ?>">
                                        <?php the_title(); ?>
                                        </a>
                                    </h3>
                                    <div class="excerpt"><?php the_excerpt(); ?></div>
                                </div>
                            </div>

                        </div>
                    <?php endwhile; ?>

            <?php endif; ?>
        </div>
    </div>
</div>


<?php get_footer() ?>
