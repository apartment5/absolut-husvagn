<?php

/*
Template Name: Drinks
 */

get_header()?>



<div class="page-content">
	<div class="container">

		<div class="row">
			<div class="col-xs-12 col-md-8 col-md-offset-2 text-center">

				<?php if ( have_posts() ):
				        while ( have_posts() ) : the_post();
				            the_content();
				        endwhile;

				    else :
				        echo 'Nothing to show';
				endif;?>
			</div>
		</div>

		<div class="row">
			<div class="drink-list col-xs-12 col-md-8 col-md-offset-2 text-center">

			<?php $page = get_the_ID();

				$the_query = new WP_Query( array(
					'sort_order' => 'asc',
					'sort_column' => 'page_title',
					'post_type' => 'page',
					'post_parent' => 39,
					'post_status' => 'publish'
				)); ?>

				<?php if ( $the_query->have_posts() ) : ?>

					<div class="row">

					<?php while ( $the_query->have_posts() ) : $the_query->the_post();

						$current_id = get_the_ID();
						$postimage = get_field('image', $current_id);?>


							<div class="drink-list-object col-xs-8 col-xs-offset-2 col-sm-6 col-sm-offset-0">

								<h3><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></h3>

								<a href="<?php the_permalink(); ?>">
									<img class="drink-image" src="<?php echo $postimage['url'];?>"/>
								</a>


								<?php if( get_field('ingredients') ):
								?>

									<div class="drink-list-ingredients">

										<?php
										unset($array);

										while ( have_rows('ingredients') ) : the_row();
											$ingredient = get_sub_field('ingredient');
											$ingredient = preg_replace('/[ ](?=[^>]*(?:<|$))/', '&nbsp', $ingredient);
											$array[] = $ingredient;


										endwhile;

										$ingredients = implode(', ', $array);
										echo $ingredients;
										?>

									</div>

								<?php endif;?>


								<a class="btn btn-primary btn-sm" href="<?php the_permalink(); ?>">Se recept <i class="fa fa-angle-right fa-lg" aria-hidden="true"></i></a>
							</div>

					<?php endwhile; ?>

					</div>

					<?php wp_reset_postdata(); ?>

				<?php else : ?>

				<?php endif; ?>
			</div>
		</div>

		<?php get_template_part( 'partials/book-part', 'page' ); ?>

	</div>
</div>


<?php get_footer() ?>
