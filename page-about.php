<?php

/*
Template Name: Om
 */

get_header() ?>

<div class="page-content">
	<div class="container">
		<div class="row">
			<div class="about-content col-xs-12 col-md-8 col-md-offset-2 text-left">
				<h2><?php the_title();?></h2>
				<?php if ( have_posts() ):
			            while ( have_posts() ) : the_post();
			                the_content();
			            endwhile;

			        else :
			            echo 'Nothing to show';
			    endif;?>

			</div>
		</div>
	</div>

</div>


<?php get_footer() ?>
