jQuery(document).ready(function($){

	var menuList = $('.menu'),
		menuItem = $('.menu a'),
		menuButton = $('.menu-btn'),
		topLine = $('.top-line'),
		middleLine = $('.middle-line'),
		bottomLine = $('.bottom-line');

	menuButton.on('click', function() {
		topLine.toggleClass('top-line-active');
		middleLine.toggleClass('middle-line-active');
		bottomLine.toggleClass('bottom-line-active');
		menuList.stop().slideToggle(250);
	});

	$('#add-vote').on('click', function() {
		var t = $(this);
		if ($.cookie('absolut-husvagn-voted-' + t.data('id')) !== 'voted') {
			$.get('?action=addVote', function(data) {
				$.cookie('absolut-husvagn-voted-' + t.data('id'), 'voted', { expires: 1 });
				$('#votes').text(parseInt($('#votes').text()) + 1);
				t.find('.fa-heart-o').hide();
				t.find('.fa-heart').removeClass('hide');
			});
		}
	});
});
