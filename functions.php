<?php
function mae_enqueue_scripts() {
    wp_enqueue_style( 'font-awesome', get_bloginfo( 'template_url' ) . '/bower_components/font-awesome/css/font-awesome.css' );

    wp_enqueue_style( 'style', get_bloginfo( 'template_url' ) . '/style.css' );

    wp_enqueue_script( 'jquery', get_bloginfo( 'template_url' ) . '/bower_components/jquery/dist/jquery.min.js', array( ));

    wp_enqueue_script( 'jqueryCookie', get_bloginfo( 'template_url' ) . '/bower_components/jquery.cookie/jquery.cookie.js', array( 'jquery' ));

    wp_enqueue_script( 'global', get_bloginfo( 'template_url' ) . '/scripts/global.js', array( ));

    wp_enqueue_script('absolut-api', 'http://avp2.absolut.com/absolut-campaign/Api', array( 'jquery' ));
    wp_enqueue_script('absolut-avp', 'http://avp2.absolut.com/bundles/age-gate?ver=20160523', array( 'jquery', 'absolut-api' ));
}

add_action( 'wp_enqueue_scripts', 'mae_enqueue_scripts' );

function mae_register_menu() {
  register_nav_menu( 'header-menu',__( 'Main Menu' ) );
}
add_action( 'init', 'mae_register_menu' );


if( function_exists('acf_add_options_page') ) {
    acf_add_options_page( 'Booking' );
    acf_add_options_page( 'Start Buttons' );
}
